library(stringr)
library(raster)
library(data.table)
names<- list.files("~/datasets/original")
split <- str_split(names,".zip")
for (i in 1:length(split)) names[i]<-split[[i]][1]
names

#for (gcm in names){
 #   print(paste("./original/",gcm,".zip",sep=""))
#    unzip(zipfile = paste("./original/",gcm,".zip",sep=""),exdir = paste0("./data/",gcm),list=F)
    
#proj <- fread(list.files(paste0("~/datasets/data/",gcm),pattern="neotropic",full.names = T))
#names(proj)
#coordinates(proj)<- ~long+lat
# gridded(proj) <- TRUE
# r <- stack(proj)
# writeRaster(r, filename = paste("./data/",gcm,"/",gcm,".tif",sep=""),bylayer=T ,suffix=names(r),overwrite=T)
# rm(proj)
# }

##### Leer los stacks 

GCM <- list()
for (i in 1:3){
  gcm <- names[i]
    vars <- stack(list.files(paste0("~/datasets/data/",gcm),pattern=".tif",full.names = T))
                     plot(vars[[1:19]]) 
                     names(vars)
                     GCM[[i]] <- vars[[1:19]]
}

GCM
for (i in 1:3) names(GCM[[i]]) <- sort(paste("bio",1:19))


plot(GCM[[2]])
