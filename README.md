This repository holds all the data and functions necessary to perform environmental niche models (ENM) using dismo and associated packages. Preexistent ENM functions in dismo() have been wrapped into a three step workflow, following both dismo and biomod() documentation.  

This is an old version, with no optimization features whatsoever.

The folder has the following structure:

+ `/data` should have the occurrence data. 

+ `/env` the predictor variables. currently these come from the 19 bioclimatic variables in Worldclim and the six topography variables in CGIAR. They have been transformed into a set of six "eigenvariables", a PCA was performed for the entire area and the first six components were spatialized, retaining 95% of the variation. The resolution of these predictors is 1km x 1km and the final extent is the BAF.

`/fct` has all the functions needed to perform the ENMs.

`modelos_old.R` defines function `dismo.mod()`, that allows to choose which predictors (as a raster::stack object) and which algoritms are used. The algorithm options are maxent, bioclim, domain, mahalanobis distance, glm, random forests and two svm implementations, and two euclidean distance implementation. All functions are implemented in dismo package https://cran.r-project.org/web/packages/dismo/index.html and the two euclidean distances are experimentally implemented by me. It also allows to choose the number of partitions (default=3), and a seed for reproducibility. ENMeval from Muscarella et al 2014 can also be run (https://cran.r-project.org/web/packages/ENMeval/ENMeval.pdf). 
The output folder is where models will be stored. The projection to other datasets is partially implemented. The number of pseudoabsences can also be modified (default=500). dismo.mod produces models for every partition but does not join them.

Joining partitions is performed by the function `final_model` in `final_model.R`. Several ways of joining partitions are used, you can look for them in `mapa.pdf` in the folder docs. There is an old tutorial in this folder with an example of how touse the functions. 


The ensemble (`ensemble.R`) function receives the species name as parameter, then the general output folder where all model were saved and the output where final models were stored. `Which.models` allows the user to select which type of models are to be averaged (options are one of the models in final.models). The function selects those models and averages them, saving them in the specified output.folder= (default= "ensemble").

Any post-processing operation may be performed manually, such as generating potential richness or checking the variation between models (by calculating the standard deviation between algorithms, for example).

*This is an outdated version of a repository that is currently being optimized for parallel computing and the use of the lowest memory possible so please use it at your own risk*

We currently have no pubication to be cited but please cite the repository address if you ever use this. Please cite `dismo` and any packages that dismo uses, such as `RandomForest` and `kernlab` and `e1071`. I will put the whole list of sources as soon as I can.
